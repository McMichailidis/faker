package org.bitbucket.mcmichailidis.faker;

import org.bitbucket.mcmichailidis.faker.providers.CardProvider;
import org.bitbucket.mcmichailidis.faker.providers.ColorProvider;
import org.bitbucket.mcmichailidis.faker.providers.CountryCodeProvider;
import org.bitbucket.mcmichailidis.faker.providers.CurrencyProvider;
import org.bitbucket.mcmichailidis.faker.providers.LanguageCodeProvider;
import org.bitbucket.mcmichailidis.faker.providers.LocaleProvider;
import org.bitbucket.mcmichailidis.faker.providers.NameProvider;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Parameter;
import java.util.Objects;
import java.util.Random;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ExtensionContext.Namespace;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

/**
 * @author KuroiKage.
 */
@SuppressWarnings({"SpellCheckingInspection", "Duplicates"})
public class WordFaker implements ParameterResolver {
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.PARAMETER)
    public @interface RandomWord {
        /**
         * The type of the word that will be produced.
         *
         * @return the produced word.
         */
        WordType type();
    }

    private interface Worker {
        Object work(Random random);
    }

    public enum WordType implements Worker {
        /**
         * A male's first name.
         */
        MALE_NAME {
            @Override
            public Object work(Random random) {
                return NameProvider.provideMaleFirstName(random);
            }
        },
        /**
         * A female's first name.
         */
        FEMALE_NAME {
            @Override
            public Object work(Random random) {
                return NameProvider.provideFemaleFirstName(random);
            }
        },
        /**
         * A full name which can be either male or female.
         */
        FULL_NAME {
            @Override
            public Object work(Random random) {
                return NameProvider.provideFullName(random);
            }
        },
        /**
         * A last name.
         */
        LAST_NAME {
            @Override
            public Object work(Random random) {
                return NameProvider.provideLastName(random);
            }
        },
        /**
         * An email from actual provider ( ex: @gmail.com ).
         */
        EMAIL {
            @Override
            public Object work(Random random) {
                return NameProvider.provideEmail(random);
            }
        },
        /**
         * An email with fake provider ( @example.com ).
         */
        SAFE_EMAIL {
            @Override
            public Object work(Random random) {
                return NameProvider.provideSafeEmail(random);
            }
        },
        /**
         * The code of a language.
         */
        LANGUAGE_CODE {
            @Override
            public Object work(Random random) {
                return LanguageCodeProvider.provide(random);
            }
        },
        /**
         * The code of a country ( 2 letters ).
         */
        COUNTRY_CODE {
            @Override
            public Object work(Random random) {
                return CountryCodeProvider.provide(random);
            }
        },
        /**
         * The code of a country according to Alpha3 ISO format ( 3 letters ).
         */
        COUNTRY_CODE_ISO_ALPHA3 {
            @Override
            public Object work(Random random) {
                return CountryCodeProvider.provideIsoAlpha3(random);
            }
        },
        /**
         * A locale.
         */
        LOCALE {
            @Override
            public Object work(Random random) {
                return LocaleProvider.provide(random);
            }
        },
        /**
         * A currency identification ( ex: EUR ).
         */
        CURRENCY {
            @Override
            public Object work(Random random) {
                return CurrencyProvider.provide(random);
            }
        },
        /**
         * A color name.
         */
        COLOR {
            @Override
            public Object work(Random random) {
                return ColorProvider.provide(random);
            }
        },
        /**
         * A color in rgb format ( R,G,B )
         */
        COLOR_RGB {
            @Override
            public Object work(Random random) {
                return ColorProvider.provideRGB(random);
            }
        },
        /**
         * A color in hex format ( #xxxxxx )
         */
        COLOR_HEX {
            @Override
            public Object work(Random random) {
                return ColorProvider.provideHEX(random);
            }
        },
        /**
         * A card vendor name.
         */
        CARD_VENDOR {
            @Override
            public Object work(Random random) {
                return CardProvider.provideVendor(random);
            }
        },
        /**
         * A valid card number of a random vendor ( valid as is "valid template" )
         */
        CARD_NUMBER {
            @Override
            public Object work(Random random) {
                return CardProvider.provideCardNumber(random);
            }
        }

    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        return parameterContext.isAnnotated(RandomWord.class);
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        Parameter parameter = parameterContext.getParameter();
        Class<?> type = parameter.getType();

        if (!String.class.equals(type)) {
            // log.error("Called WordFaker while the type is not string.");
            return null;
        }

        Annotation[] declaredAnnotations = parameter.getDeclaredAnnotations();
        RandomWord self = null;

        for (Annotation vL : declaredAnnotations) {
            if (vL instanceof RandomWord) {
                self = (RandomWord) vL;
            }
        }

        if (Objects.isNull(self)) {
            // log.info("The field was not annotated with RandomWord");
            return null;
        }

        Random random = extensionContext.getRoot().getStore(Namespace.GLOBAL)
                .getOrComputeIfAbsent(java.util.Random.class);

        return self.type().work(random);
    }
}

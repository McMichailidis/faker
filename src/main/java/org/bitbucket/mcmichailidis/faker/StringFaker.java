package org.bitbucket.mcmichailidis.faker;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Parameter;
import java.util.Objects;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

/**
 * @author KuroiKage.
 */
@SuppressWarnings("Duplicates")
public class StringFaker implements ParameterResolver {
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.PARAMETER)
    public @interface RandomStr {
        /**
         * Replaces the random value with the provided one.
         *
         * @return the provided value.
         */
        String value() default "";

        /**
         * The length of the random string.
         *
         * @return the random string length.
         */
        int length() default 10;

        /**
         * If the returned string will contain numbers.
         *
         * @return true if it should use numbers. else false
         */
        boolean hasNumbers() default false;

        /**
         * If the returned string will contain characters.
         *
         * @return true if it should use characters. else false
         */
        boolean hasCharacters() default true;

        /**
         * The characters that will be used to create the string.
         *
         * @return the characters to use.
         */
        char[] charsToUse() default {};
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        return parameterContext.isAnnotated(RandomStr.class);
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        Parameter parameter = parameterContext.getParameter();
        Class<?> type = parameter.getType();

        if (!String.class.equals(type)) {
            // log.error("Called StringFaker while the type is not string.");
            return null;
        }

        Annotation[] declaredAnnotations = parameter.getDeclaredAnnotations();
        RandomStr self = null;

        for (Annotation vL : declaredAnnotations) {
            if (vL instanceof RandomStr) {
                self = (RandomStr) vL;
            }
        }

        if (Objects.isNull(self)) {
            // log.info("The field was not annotated with RandomStr");
            return null;
        }

        char[] chars = self.charsToUse();

        boolean forceCharacters = false;
        if (!self.hasNumbers() && !self.hasCharacters()) {
            forceCharacters = true;
        }

        return !self.value().equals("") ? self.value() :
                chars.length == 0
                        ? RandomStringUtils.random(self.length(), 0, 0, forceCharacters || self.hasCharacters(),
                        self.hasNumbers())
                        : RandomStringUtils.random(self.length(), 0, chars.length, forceCharacters || self.hasCharacters(),
                        self.hasNumbers(),
                        chars);

    }
}

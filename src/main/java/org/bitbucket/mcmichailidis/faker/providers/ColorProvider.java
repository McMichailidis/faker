package org.bitbucket.mcmichailidis.faker.providers;

import java.util.Random;

/**
 * @author KuroiKage.
 */
public final class ColorProvider {
    private final static String[] colors = {
            "AliceBlue", "AntiqueWhite", "Aqua", "Aquamarine",
            "Azure", "Beige", "Bisque", "Black", "BlanchedAlmond",
            "Blue", "BlueViolet", "Brown", "BurlyWood", "CadetBlue",
            "Chartreuse", "Chocolate", "Coral", "CornflowerBlue",
            "Cornsilk", "Crimson", "Cyan", "DarkBlue", "DarkCyan",
            "DarkGoldenRod", "DarkGray", "DarkGreen", "DarkKhaki",
            "DarkMagenta", "DarkOliveGreen", "Darkorange", "DarkOrchid",
            "DarkRed", "DarkSalmon", "DarkSeaGreen", "DarkSlateBlue",
            "DarkSlateGray", "DarkTurquoise", "DarkViolet", "DeepPink",
            "DeepSkyBlue", "DimGray", "DimGrey", "DodgerBlue", "FireBrick",
            "FloralWhite", "ForestGreen", "Fuchsia", "Gainsboro", "GhostWhite",
            "Gold", "GoldenRod", "Gray", "Green", "GreenYellow", "HoneyDew",
            "HotPink", "IndianRed", "Indigo", "Ivory", "Khaki", "Lavender",
            "LavenderBlush", "LawnGreen", "LemonChiffon", "LightBlue", "LightCoral",
            "LightCyan", "LightGoldenRodYellow", "LightGray", "LightGreen", "LightPink",
            "LightSalmon", "LightSeaGreen", "LightSkyBlue", "LightSlateGray", "LightSteelBlue",
            "LightYellow", "Lime", "LimeGreen", "Linen", "Magenta", "Maroon", "MediumAquaMarine",
            "MediumBlue", "MediumOrchid", "MediumPurple", "MediumSeaGreen", "MediumSlateBlue",
            "MediumSpringGreen", "MediumTurquoise", "MediumVioletRed", "MidnightBlue",
            "MintCream", "MistyRose", "Moccasin", "NavajoWhite", "Navy", "OldLace", "Olive",
            "OliveDrab", "Orange", "OrangeRed", "Orchid", "PaleGoldenRod", "PaleGreen",
            "PaleTurquoise", "PaleVioletRed", "PapayaWhip", "PeachPuff", "Peru", "Pink", "Plum",
            "PowderBlue", "Purple", "Red", "RosyBrown", "RoyalBlue", "SaddleBrown", "Salmon",
            "SandyBrown", "SeaGreen", "SeaShell", "Sienna", "Silver", "SkyBlue", "SlateBlue",
            "SlateGray", "Snow", "SpringGreen", "SteelBlue", "Tan", "Teal", "Thistle", "Tomato",
            "Turquoise", "Violet", "Wheat", "White", "WhiteSmoke", "Yellow", "YellowGreen"
    };

    public static String provide(Random random) {
        return colors[random.nextInt(colors.length)];
    }

    public static String provideRGB(Random random) {
        int red = random.nextInt(256);
        int green = random.nextInt(256);
        int blue = random.nextInt(256);

        return red + "," + green + "," + blue;
    }

    public static String provideHEX(Random random) {
        return "#" + Integer.toHexString(random.nextInt(16777216));
    }
}

package org.bitbucket.mcmichailidis.faker.providers;

import java.util.Random;

/**
 * @author KuroiKage.
 */
public final class CardProvider {
    private final static String[] cardVendors = {
            "Visa", "MasterCard", "American Express", "Discover Card"
    };

    private final static String[] visaMask = {
            "4539########",
            "4539###########",
            "4556########",
            "4556###########",
            "4916########",
            "4916###########",
            "4532########",
            "4532###########",
            "4929########",
            "4929###########",
            "40240071####",
            "40240071#######",
            "4485########",
            "4485###########",
            "4716########",
            "4716###########",
            "4###########",
            "4##############"
    };

    private final static String[] masterCardMask = {
            "2221###########",
            "23#############",
            "24#############",
            "25#############",
            "26#############",
            "2720###########",
            "51#############",
            "52#############",
            "53#############",
            "54#############",
            "55#############"
    };

    private final static String[] americanExpressMask = {
            "34############",
            "37############"
    };

    private final static String[] discoverMask = {"6011###########"};

    public static String provideVendor(Random random) {
        return cardVendors[random.nextInt(cardVendors.length)];
    }

    public static String provideCardNumber(Random random) {
        switch (provideVendor(random)) {
            case "Visa":
                return randomizePlaceholders(visaMask[random.nextInt(visaMask.length)], random);
            case "MasterCard":
                return randomizePlaceholders(masterCardMask[random.nextInt(masterCardMask.length)], random);
            case "American Express":
                return randomizePlaceholders(americanExpressMask[random.nextInt(americanExpressMask.length)], random);
            default:
                return randomizePlaceholders(discoverMask[random.nextInt(discoverMask.length)], random);
        }
    }

    private static String randomizePlaceholders(String str, Random random) {
        boolean flag = true;

        while (flag) {
            String replace = str.replace("#", String.valueOf(random.nextInt(10)));
            if (replace.equals(str)) {
                flag = false;
            } else {
                str = replace;
            }
        }

        return str;
    }
}

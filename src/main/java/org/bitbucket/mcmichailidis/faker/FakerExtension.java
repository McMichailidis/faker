package org.bitbucket.mcmichailidis.faker;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

/**
 * @author KuroiKage.
 */
@SuppressWarnings("SpellCheckingInspection")
public class FakerExtension implements ParameterResolver {

    private static final List<ParameterResolver> customAnnotations;

    static {
        customAnnotations = new ArrayList<>();

        customAnnotations.add(new IntegerFaker());
        customAnnotations.add(new StringFaker());
        customAnnotations.add(new WordFaker());
        customAnnotations.add(new UrlFaker());
    }

    public static void registerCustomResovler(ParameterResolver parameterResolver) {
        customAnnotations.add(parameterResolver);
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        return true;
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        Annotation[] annotations = parameterContext.getParameter().getAnnotations();

        if (annotations.length < 1) {
            return null;
        }

        for (ParameterResolver pR : customAnnotations) {
            if (pR.supportsParameter(parameterContext, extensionContext)) {
                return pR.resolveParameter(parameterContext, extensionContext);
            }
        }

        return null;
    }
}

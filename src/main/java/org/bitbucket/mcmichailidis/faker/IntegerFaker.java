package org.bitbucket.mcmichailidis.faker;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Parameter;
import java.util.Objects;
import java.util.Random;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ExtensionContext.Namespace;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

/**
 * @author KuroiKage.
 */
public class IntegerFaker implements ParameterResolver {
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.PARAMETER)
    public @interface RandomInt {
        /**
         * Replaces the random value with the provided one.
         *
         * @return the provided value.
         */
        int value() default -1;

        /**
         * The max value for the random int.
         *
         * @return the max value of the random int.
         */
        int max() default -1;

        /**
         * The sign of the returned value.
         *
         * @return the chosen sign. Default ANY
         */
        Sign sign() default Sign.ANY;
    }

    private interface Converter {
        int apply(int i);
    }

    public enum Sign implements Converter {
        POSITIVE {
            @Override
            public int apply(int i) {
                return i < 0 ? -i : i;
            }
        },
        NEGATIVE {
            @Override
            public int apply(int i) {
                return i > 0 ? -i : i;
            }
        },
        ANY {
            @Override
            public int apply(int i) {
                return i;
            }
        }
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        return parameterContext.isAnnotated(RandomInt.class);
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        Parameter parameter = parameterContext.getParameter();
        Class<?> type = parameter.getType();

        if (!int.class.equals(type)) {
            // log.error("Called IntegerFaker while the type is not string.");
            return null;
        }

        Annotation[] declaredAnnotations = parameter.getDeclaredAnnotations();
        RandomInt self = null;

        for (Annotation vL : declaredAnnotations) {
            if (vL instanceof RandomInt) {
                self = (RandomInt) vL;
            }
        }

        if (Objects.isNull(self)) {
            // log.info("The field was not annotated with RandomInt");
            return null;
        }

        Random random = extensionContext.getRoot().getStore(Namespace.GLOBAL)
                .getOrComputeIfAbsent(java.util.Random.class);

        return self.value() != -1
                ? self.value()
                : self.sign()
                .apply(self.max() == -1
                        ? random.nextInt()
                        : random.nextInt(self.max() > 32 ? 32 : self.max()));
    }
}

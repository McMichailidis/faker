package org.bitbucket.mcmichailidis.faker;


import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Parameter;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Objects;
import java.util.Random;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ExtensionContext.Namespace;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

/**
 * @author KuroiKage.
 */
public class UrlFaker implements ParameterResolver {
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.PARAMETER)
    public @interface RandomUrl {

        /**
         * The protocol which the url will have.
         *
         * @return the chosen protocol. Default is ANY.
         */
        Protocol protocol() default Protocol.ANY;

        /**
         * A user-provided protocol in case the chosen
         * protocol was Custom.
         *
         * @return the user provided protocol.
         */
        String customProtocol() default "";

        /**
         * The url suffix.
         *
         * @return the suffix for the url. Default is ".com".
         */
        String suffix() default ".com";

        /**
         * Should include www or not to the url.
         *
         * @return true if it should include www else false. Default is true.
         */
        boolean hasWWW() default true;

        /**
         * Should include a path to the url or not.
         *
         * @return true to include else false.
         */
        boolean hasPath() default false;

        /**
         * Should include a get request encoded in the url or not.
         *
         * @return true to include else false.
         */
        boolean hasGetRequest() default false;
    }

    private interface Converter {
        String get(Random random);
    }

    public enum Protocol implements Converter {
        NONE {
            @Override
            public String get(Random random) {
                return "";
            }
        },
        ANY {
            @Override
            public String get(Random random) {
                switch (random.nextInt(4)) {
                    case 0:
                        return HTTP.get(random);
                    case 1:
                        return HTTPS.get(random);
                    default:
                        return FTP.get(random);
                }
            }
        },
        HTTP {
            @Override
            public String get(Random random) {
                return "http://";
            }
        },
        HTTPS {
            @Override
            public String get(Random random) {
                return "https://";
            }
        },
        FTP {
            @Override
            public String get(Random random) {
                return "ftp://";
            }
        },
        CUSTOM {
            @Override
            public String get(Random random) {
                return "";
            }
        }
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        return parameterContext.isAnnotated(RandomUrl.class);
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        Parameter parameter = parameterContext.getParameter();
        Class<?> type = parameter.getType();

        if (!String.class.equals(type)
                && !URL.class.equals(type)
                && !URI.class.equals(type)) {
            // log.error("Called StringFaker while the type is not string.");
            return null;
        }

        Annotation[] declaredAnnotations = parameter.getDeclaredAnnotations();
        RandomUrl self = null;

        for (Annotation vL : declaredAnnotations) {
            if (vL instanceof RandomUrl) {
                self = (RandomUrl) vL;
            }
        }

        if (Objects.isNull(self)) {
            // log.info("The field was not annotated with RandomStr");
            return null;
        }

        StringBuilder str = new StringBuilder();

        Random random = extensionContext.getRoot().getStore(Namespace.GLOBAL)
                .getOrComputeIfAbsent(java.util.Random.class);

        str.append(self.protocol().equals(Protocol.CUSTOM) ? self.customProtocol() : self.protocol().get(random));
        str.append(self.hasWWW() ? "www." : "");
        str.append(RandomStringUtils.randomAlphabetic(random.nextInt(9) + 1));
        str.append(self.suffix().startsWith(".") ? self.suffix() : "." + self.suffix());
        str.append(self.hasPath() ? getPath(random) : (self.hasGetRequest() ? "/" : ""));
        str.append(self.hasGetRequest() ? getRequest(random) : "");

        if (URL.class.equals(type)) {
            try {
                return new URL(str.toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        if (URI.class.equals(type)) {
            return URI.create(str.toString());
        }

        return str.toString();
    }

    private String getRequest(Random random) {
        int i = random.nextInt(3) + 1;
        StringBuilder str = new StringBuilder("?");
        for (int j = 0; j <= i; j++) {
            str.append(RandomStringUtils.randomAlphabetic(random.nextInt(3) + 1))
                    .append("=")
                    .append(RandomStringUtils.randomAlphabetic(random.nextInt(6) + 1))
                    .append(j == i ? "" : "&");
        }
        return str.toString();

    }

    private String getPath(Random random) {
        int i = random.nextInt(3) + 1;
        StringBuilder str = new StringBuilder();
        for (int j = 0; j <= i; j++) {
            str.append("/")
                    .append(RandomStringUtils.randomAlphabetic(random.nextInt(9) + 1));
        }
        return str.toString();
    }
}

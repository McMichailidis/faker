package org.bitbucket.mcmichailidis.faker;

import org.bitbucket.mcmichailidis.faker.WordFaker.RandomWord;
import org.bitbucket.mcmichailidis.faker.WordFaker.WordType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

/**
 * @author KuroiKage.
 */
@ExtendWith(FakerExtension.class)
class WordFakerTest {

    @Test
    void demo(@RandomWord(type = WordType.MALE_NAME) String maleName,
              @RandomWord(type = WordType.FEMALE_NAME) String femaleName,
              @RandomWord(type = WordType.FULL_NAME) String fullName,
              @RandomWord(type = WordType.LAST_NAME) String lastName,
              @RandomWord(type = WordType.EMAIL) String email,
              @RandomWord(type = WordType.SAFE_EMAIL) String safeEmail,
              @RandomWord(type = WordType.LANGUAGE_CODE) String languageCode,
              @RandomWord(type = WordType.COUNTRY_CODE) String countryCode,
              @RandomWord(type = WordType.COUNTRY_CODE_ISO_ALPHA3) String countryCodeAlpha,
              @RandomWord(type = WordType.LOCALE) String locale,
              @RandomWord(type = WordType.CURRENCY) String currency,
              @RandomWord(type = WordType.COLOR) String color,
              @RandomWord(type = WordType.COLOR_RGB) String colorRgb,
              @RandomWord(type = WordType.COLOR_HEX) String colorHex,
              @RandomWord(type = WordType.CARD_VENDOR) String cardVendor,
              @RandomWord(type = WordType.CARD_NUMBER) String cardNumber
    ) {
        System.out.println();
    }

}
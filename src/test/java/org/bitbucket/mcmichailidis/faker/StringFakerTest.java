package org.bitbucket.mcmichailidis.faker;

import org.bitbucket.mcmichailidis.faker.StringFaker.RandomStr;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.extension.ExtendWith;

/**
 * @author KuroiKage.
 */
@ExtendWith(StringFaker.class)
class StringFakerTest {
    @RepeatedTest(100)
    void demo(@RandomStr String first,
              @RandomStr(hasNumbers = true) String hasNumbers,
              @RandomStr(hasCharacters = false, hasNumbers = true) String onlyNumbers,
              @RandomStr(length = 4) String smallString,
              @RandomStr(length = 50) String hugeString,
              @RandomStr(value = "def") String defaultValue,
              @RandomStr(charsToUse = {'a', 'b', 'c', 'D', '1'}) String setOfChars,
              @RandomStr(hasCharacters = false) String forceCharacters) {

        Assertions.assertEquals(4, smallString.length());
        Assertions.assertEquals(50, hugeString.length());
        Assertions.assertEquals("def", defaultValue);

        // Removed because of the chances that there will be no numbers
        //
        // boolean hasNumber = false;
        // boolean hasLetter = false;
        // for (char vL : hasNumbers.toCharArray()) {
        //     if (Character.isDigit(vL)) {
        //         hasNumber = true;
        //     }
        //     if (Character.isLetter(vL)) {
        //         hasLetter = true;
        //     }
        //
        // }
        //
        // Assertions.assertTrue(hasNumber && hasLetter, hasNumbers);

        for (char vL : onlyNumbers.toCharArray()) {
            Assertions.assertTrue(Character.isDigit(vL));
        }

        List<Character> list = new ArrayList<>();
        list.add('a');
        list.add('b');
        list.add('c');
        list.add('D');
        list.add('1');

        for (char vL : setOfChars.toCharArray()) {
            Assertions.assertTrue(list.contains(vL));
        }

        for (char vL : forceCharacters.toCharArray()) {
            Assertions.assertTrue(Character.isLetter(vL));
        }
    }
}
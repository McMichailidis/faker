package org.bitbucket.mcmichailidis.faker;

import java.net.URI;
import java.net.URL;
import org.bitbucket.mcmichailidis.faker.UrlFaker.Protocol;
import org.bitbucket.mcmichailidis.faker.UrlFaker.RandomUrl;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.extension.ExtendWith;

/**
 * @author KuroiKage.
 */
@SuppressWarnings("SpellCheckingInspection")
@ExtendWith(FakerExtension.class)
class UrlFakerTest {
    @RepeatedTest(100)
    void demo(@RandomUrl String baseUrl,
              @RandomUrl(customProtocol = "smth") String ignoreProtocol,
              @RandomUrl(protocol = Protocol.HTTPS) String httpsProtocol,
              @RandomUrl(protocol = Protocol.NONE) String noneProtocol,
              @RandomUrl(protocol = Protocol.ANY) String anyProtocol,
              @RandomUrl(protocol = Protocol.HTTP) String httpProtocol,
              @RandomUrl(protocol = Protocol.FTP) String ftpProtocol,
              @RandomUrl(protocol = Protocol.CUSTOM, customProtocol = "MINE://") String customProtocol,
              @RandomUrl(hasWWW = false) String noWWW,
              @RandomUrl(hasPath = true) String hasPath,
              @RandomUrl(hasPath = true, hasGetRequest = true) String hasPathAndRequest,
              @RandomUrl(suffix = ".gr") String customSuffix,
              @RandomUrl(hasGetRequest = true) String hasOnlyRequest,

              @RandomUrl URL baseUrlURL,
              @RandomUrl(customProtocol = "smth") URL ignoreProtocolURL,
              @RandomUrl(protocol = Protocol.HTTPS) URL httpsProtocolURL,
              // @RandomUrl(protocol = Protocol.NONE) URL noneProtocolURL,  MALFORMED
              @RandomUrl(protocol = Protocol.ANY) URL anyProtocolURL,
              @RandomUrl(protocol = Protocol.HTTP) URL httpProtocolURL,
              @RandomUrl(protocol = Protocol.FTP) URL ftpProtocolURL,
              // @RandomUrl(protocol = Protocol.CUSTOM, customProtocol = "MINE://") URL customProtocolURL,  MALFORMED
              @RandomUrl(hasWWW = false) URL noWWWURL,
              @RandomUrl(hasPath = true) URL hasPathURL,
              @RandomUrl(hasPath = true, hasGetRequest = true) URL hasPathAndRequestURL,
              @RandomUrl(suffix = ".gr") URL customSuffixURL,
              @RandomUrl(hasGetRequest = true) URL hasOnlyRequestURL,

              @RandomUrl URI baseUrlURI,
              @RandomUrl(customProtocol = "smth") URI ignoreProtocolURI,
              @RandomUrl(protocol = Protocol.HTTPS) URI httpsProtocolURI,
              @RandomUrl(protocol = Protocol.NONE) URI noneProtocolURI,
              @RandomUrl(protocol = Protocol.ANY) URI anyProtocolURI,
              @RandomUrl(protocol = Protocol.HTTP) URI httpProtocolURI,
              @RandomUrl(protocol = Protocol.FTP) URI ftpProtocolURI,
              @RandomUrl(protocol = Protocol.CUSTOM, customProtocol = "MINE://") URI customProtocolURI,
              @RandomUrl(hasWWW = false) URI noWWWURI,
              @RandomUrl(hasPath = true) URI hasPathURI,
              @RandomUrl(hasPath = true, hasGetRequest = true) URI hasPathAndRequestURI,
              @RandomUrl(suffix = ".gr") URI customSuffixURI,
              @RandomUrl(hasGetRequest = true) URI hasOnlyRequestURI

    ) {
        System.out.println();
    }
}
package org.bitbucket.mcmichailidis.faker;

import org.bitbucket.mcmichailidis.faker.IntegerFaker.RandomInt;
import org.bitbucket.mcmichailidis.faker.IntegerFaker.Sign;
import org.bitbucket.mcmichailidis.faker.StringFaker.RandomStr;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

/**
 * @author KuroiKage.
 */
// @ExtendWith(IntegerFaker.class)
@ExtendWith(FakerExtension.class)
class IntegerFakerTest {
    @Test
    void demo(@RandomInt int random,
              @RandomInt(5) int valued,
              @RandomInt(max = 10, sign = Sign.POSITIVE) int withMax,
              @RandomInt(value = 4, max = 20) int withBoth,
              @RandomInt(sign = Sign.POSITIVE) int onlyPositive,
              @RandomInt(sign = Sign.NEGATIVE) int onlyNegative,
              @RandomStr String demo,
              String demo2) {

        Assertions.assertEquals(5, valued);
        Assertions.assertTrue(withMax < 10);

        Assertions.assertTrue(withMax >= 0);

        Assertions.assertEquals(4, withBoth);
        Assertions.assertTrue(onlyPositive > 0);
        Assertions.assertTrue(onlyNegative < 0);
    }
}